package com.alexnae.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "pedidos_producto", schema = "hiberkebab", catalog = "")
public class PedidosProducto {
    private int idPp;

    @Id
    @Column(name = "id_pp")
    public int getIdPp() {
        return idPp;
    }

    public void setIdPp(int idPp) {
        this.idPp = idPp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PedidosProducto that = (PedidosProducto) o;
        return idPp == that.idPp;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPp);
    }
}
