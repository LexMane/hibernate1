package com.alexnae.hibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "pedidos", schema = "hiberkebab", catalog = "")
public class Pedido {
    private int idPedido;
    private Date fechaPedido;
    private byte domicilio;
    private byte pagado;
    private Empleado empleado;
    private List<Producto> productos;

    @Id
    @Column(name = "id_pedido")
    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    @Basic
    @Column(name = "fecha_pedido")
    public Date getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(Date fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    @Basic
    @Column(name = "domicilio")
    public byte getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(byte domicilio) {
        this.domicilio = domicilio;
    }

    @Basic
    @Column(name = "pagado")
    public byte getPagado() {
        return pagado;
    }

    public void setPagado(byte pagado) {
        this.pagado = pagado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pedido pedido = (Pedido) o;
        return idPedido == pedido.idPedido &&
                domicilio == pedido.domicilio &&
                pagado == pedido.pagado &&
                Objects.equals(fechaPedido, pedido.fechaPedido);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPedido, fechaPedido, domicilio, pagado);
    }

    @ManyToOne
    @JoinColumn(name = "id_empleado", referencedColumnName = "id_empleado", nullable = false)
    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    @ManyToMany
    @JoinTable(name = "pedidos_producto", catalog = "", schema = "hiberkebab", joinColumns = @JoinColumn(name = "id_producto", referencedColumnName = "id_pedido", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_pedido", referencedColumnName = "id_producto", nullable = false))
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }
}
