package com.alexnae.hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Local {
    private int idLocal;
    private String nombreLocal;
    private String ciudad;
    private int codigoPostal;
    private List<Empleado> empleados;

    @Id
    @Column(name = "id_local")
    public int getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(int idLocal) {
        this.idLocal = idLocal;
    }

    @Basic
    @Column(name = "nombre_local")
    public String getNombreLocal() {
        return nombreLocal;
    }

    public void setNombreLocal(String nombreLocal) {
        this.nombreLocal = nombreLocal;
    }

    @Basic
    @Column(name = "ciudad")
    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    @Basic
    @Column(name = "codigo_postal")
    public int getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Local local = (Local) o;
        return idLocal == local.idLocal &&
                codigoPostal == local.codigoPostal &&
                Objects.equals(nombreLocal, local.nombreLocal) &&
                Objects.equals(ciudad, local.ciudad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idLocal, nombreLocal, ciudad, codigoPostal);
    }

    @OneToMany(mappedBy = "local")
    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }
}
